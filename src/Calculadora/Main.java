package Calculadora;
import java.util.Scanner;//Import Scanner

class Calculo{
	
	public int num1;
	public int num2;
	public int operacao;
	
	public int adicao() {
		return num1+num2;
	}
	
	public int subtracao() {
		return num1 - num2;
	}
	
	public int multiplicacao() {
		return num1 * num2;
	}
	
	public int divisao() {
		return num1 / num2;
	}
}

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);//add Scanner
		Calculo cal = new Calculo();
		
		int restart = 1;
		
		do {
			System.out.println("Digite o primeiro n�mero");
			cal.num1 = in.nextInt();
			
			System.out.println("Digite o segundo n�mero");
			cal.num2 = in.nextInt();
			
			System.out.println("Selecione a opera��o:");
			System.out.println("1 para Adi��o");
			System.out.println("2 para Subtra��o");
			System.out.println("3 para Multiplica��o");
			System.out.println("4 para Divis�o");
			cal.operacao = in.nextInt();
			
			if(cal.operacao == 1) {
				System.out.println(cal.adicao());
			}else if (cal.operacao == 2) {
				System.out.println(cal.subtracao());
			}else if (cal.operacao == 3) {
				System.out.println(cal.multiplicacao());
			}else if(cal.operacao == 4) {
				System.out.println(cal.divisao());
			}
			
			System.out.println("Deseja continuar?: 1- sim/2- n�o");
			restart = in.nextInt();
			
		}while(restart == 1);
	}
	

}
